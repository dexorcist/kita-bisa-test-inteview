package com.test;

import java.util.Scanner;

public class Main {
    private static AnswerClass answerClass = new AnswerClass();
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean isRepeat = true;
        double x = 0, y = 0;
        int n = 0;
        do {
            System.out.println("\n==============================");
            System.out.println("== Kita Bisa Test Interview ==");
            System.out.println("==============================");
            System.out.println("1. Sum\n2. Multiply\n3. Prime Number\n4. Fibonacci Sequence\n5. Exit");

            System.out.print("please Choose : ");
            String choose = scanner.nextLine();

            switch (choose){
                case "1" :
                    System.out.println("\n==============================");
                    System.out.println("=========== SUM ==============");
                    System.out.println("==============================");
                    System.out.print("Number X : ");
                    x = scanner.nextDouble();
                    System.out.print("Number Y : ");
                    y = scanner.nextDouble();
                    System.out.println("Result : " + answerClass.sum(x,y));
                    break;
                case "2" :
                    System.out.println("\n==============================");
                    System.out.println("========= Multiply ===========");
                    System.out.println("==============================");
                    System.out.print("Number X : ");
                    x = scanner.nextDouble();
                    System.out.print("Number Y : ");
                    y = scanner.nextDouble();
                    System.out.println("Result : " + answerClass.multiply(x,y));
                    break;
                case "3" :
                    System.out.println("\n==============================");
                    System.out.println("======= Prime Number =========");
                    System.out.println("==============================");
                    System.out.print("N Number : ");
                    n = scanner.nextInt();
                    System.out.println("Result : " + answerClass.primeNumber(n));
                    break;
                case "4" :
                    System.out.println("\n==============================");
                    System.out.println("===== Fibonacci sequence =====");
                    System.out.println("==============================");
                    System.out.print("N Number : ");
                    n = scanner.nextInt();
                    System.out.println("Result : " + answerClass.fibonacciSequence(n));
                    break;
                case "5" :
                    System.out.println("Thank You :)");
                    isRepeat = false;
                    break;
                default:
                    System.out.println("Wrong Choose :)");
                    isRepeat = false;
                    break;
            }
        scanner.nextLine();
        }while (isRepeat);
        System.exit(0);
    }
}
