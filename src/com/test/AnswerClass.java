package com.test;

import java.util.ArrayList;
import java.util.List;

public class AnswerClass {

    public double sum(double x, double  y){
        return  x + y;
    }

    public double multiply(double x, double y){
        return x * y;
    }

    public List<Integer> primeNumber(int n){
        List<Integer> result = new ArrayList<>();
        int index = 0;
        while(result.size() != n){
            if(is_prime(index))
                result.add(index);
            index++;
        }
        return result;
    }

    private static final boolean is_prime(int x){
        if(x <= 1)
            return false;
        for(int i = 2; i < x; i++){
            if(x % i == 0)
                return false;
        }
        return true;
    }

    public List<Integer> fibonacciSequence(int n){
        List<Integer> result = new ArrayList<>();
        int temp1 = 0, temp2 = 1;
        while(result.size() != n){
            result.add(temp1);
            int sum = temp1 + temp2;
            temp1 = temp2;
            temp2 = sum;
        }
        return result;
    }
}
