package com.test.jUnit;

import com.test.AnswerClass;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AnswerClassTest {
    private AnswerClass answerClass = new AnswerClass();
    @org.junit.jupiter.api.Test
    void sum() {
        assertEquals(3, answerClass.sum(1,2));
        assertEquals(6, answerClass.sum(4,2));
        assertNotEquals(8, answerClass.sum(2,3));
    }

    @org.junit.jupiter.api.Test
    void multiply() {
        assertEquals(4,answerClass.multiply(1,4));
    }

    @org.junit.jupiter.api.Test
    void primeNumber() {
        List<Integer> prime = Arrays.asList(2,3,5,7,11,13);
        assertEquals(prime, answerClass.primeNumber(6));
    }

    @org.junit.jupiter.api.Test
    void fibonacciSequence() {
        List<Integer> fibonacci  = Arrays.asList(0,1,1,2,3,5);
        assertEquals(fibonacci,answerClass.fibonacciSequence(6));
    }
}